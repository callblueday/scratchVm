// var data = {
//   "children": [
//     {
//       "costumes": [
//         {
//           "baseLayerID": -1,
//           "baseLayerMD5": "cff9ae87a93294693a0650b38a7a33d2.svg",
//           "bitmapResolution": 1,
//           "costumeName": "shark-b ",
//           "rotationCenterX": 75,
//           "rotationCenterY": 75
//         }
//       ],
//       "currentCostumeIndex": 0,
//       "direction": -56.999999999999886,
//       "indexInLibrary": 1,
//       "isDraggable": false,
//       "objName": "Sprite1",
//       "rotationStyle": "normal",
//       "scale": 2.3752974211735975,
//       "scratchX": 0,
//       "scratchY": 0,
//       "scripts": [
//         [
//           14,
//           199,
//           [
//             [
//               "whenGreenFlag"
//             ],
//             [
//               "doForever",
//               [
//                 [
//                   "setVar:to:",
//                   "x",
//                   [
//                     "+",
//                     120,
//                     [
//                       "*",
//                       120,
//                       [
//                         "computeFunction:of:",
//                         "sin",
//                         [
//                           "*",
//                           [
//                             "%",
//                             [
//                               "timer"
//                             ],
//                             6.28
//                           ],
//                           [
//                             "/",
//                             180,
//                             3.1415
//                           ]
//                         ]
//                       ]
//                     ]
//                   ]
//                 ],
//                 [
//                   "changeGraphicEffect:by:",
//                   "color",
//                   40
//                 ],
//                 [
//                   "setSizeTo:",
//                   [
//                     "readVariable",
//                     "x"
//                   ]
//                 ],
//                 [
//                   "turnRight:",
//                   80
//                 ],
//                 [
//                   "doIfElse",
//                   [
//                     "=",
//                     [
//                       "readVariable",
//                       "effect"
//                     ],
//                     "0"
//                   ],
//                   [
//                     [
//                       "setGraphicEffect:to:",
//                       "fisheye",
//                       [
//                         "readVariable",
//                         "x"
//                       ]
//                     ]
//                   ],
//                   [
//                     [
//                       "doIfElse",
//                       [
//                         "=",
//                         [
//                           "readVariable",
//                           "effect"
//                         ],
//                         "1"
//                       ],
//                       [
//                         [
//                           "setGraphicEffect:to:",
//                           "whirl",
//                           [
//                             "readVariable",
//                             "x"
//                           ]
//                         ]
//                       ],
//                       [
//                         [
//                           "doIfElse",
//                           [
//                             "=",
//                             [
//                               "readVariable",
//                               "effect"
//                             ],
//                             "2"
//                           ],
//                           [
//                             [
//                               "setGraphicEffect:to:",
//                               "pixelate",
//                               [
//                                 "readVariable",
//                                 "x"
//                               ]
//                             ]
//                           ],
//                           [
//                             [
//                               "setGraphicEffect:to:",
//                               "mosaic",
//                               [
//                                 "readVariable",
//                                 "x"
//                               ]
//                             ]
//                           ]
//                         ]
//                       ]
//                     ]
//                   ]
//                 ]
//               ]
//             ]
//           ]
//         ],
//         [
//           328,
//           355,
//           [
//             [
//               "whenGreenFlag"
//             ],
//             [
//               "setVar:to:",
//               "effect",
//               0
//             ],
//             [
//               "doForever",
//               [
//                 [
//                   "wait:elapsed:from:",
//                   2
//                 ],
//                 [
//                   "filterReset"
//                 ],
//                 [
//                   "changeVar:by:",
//                   "effect",
//                   1
//                 ],
//                 [
//                   "setVar:to:",
//                   "effect",
//                   [
//                     "%",
//                     [
//                       "readVariable",
//                       "effect"
//                     ],
//                     4
//                   ]
//                 ]
//               ]
//             ]
//           ]
//         ],
//         [
//           273,
//           614,
//           [
//             [
//               "whenGreenFlag"
//             ],
//             [
//               "doForever",
//               [
//                 [
//                   "forward:",
//                   [
//                     "-",
//                     200,
//                     [
//                       "readVariable",
//                       "x"
//                     ]
//                   ]
//                 ],
//                 [
//                   "bounceOffEdge"
//                 ]
//               ]
//             ]
//           ]
//         ]
//       ],
//       "sounds": [
//         {
//           "format": "",
//           "md5": "83c36d806dc92327b9e7049a565c6bff.wav",
//           "rate": 22050,
//           "sampleCount": 18688,
//           "soundID": -1,
//           "soundName": "meow"
//         }
//       ],
//       "spriteInfo": {},
//       "variables": [
//         {
//           "isPersistent": false,
//           "name": "effect",
//           "value": 2
//         }
//       ],
//       "visible": true
//     },
//     {
//       "cmd": "getVar:",
//       "color": 15629590,
//       "isDiscrete": true,
//       "label": "vy",
//       "mode": 1,
//       "param": "vy",
//       "sliderMax": 100,
//       "sliderMin": 0,
//       "target": "Stage",
//       "visible": false,
//       "x": 5,
//       "y": 5
//     },
//     {
//       "cmd": "getVar:",
//       "color": 15629590,
//       "isDiscrete": true,
//       "label": "g",
//       "mode": 1,
//       "param": "g",
//       "sliderMax": 100,
//       "sliderMin": 0,
//       "target": "Stage",
//       "visible": false,
//       "x": 5,
//       "y": 32
//     },
//     {
//       "cmd": "getVar:",
//       "color": 15629590,
//       "isDiscrete": true,
//       "label": "vx",
//       "mode": 1,
//       "param": "vx",
//       "sliderMax": 100,
//       "sliderMin": 0,
//       "target": "Stage",
//       "visible": false,
//       "x": 5,
//       "y": 59
//     },
//     {
//       "cmd": "getVar:",
//       "color": 15629590,
//       "isDiscrete": true,
//       "label": "i",
//       "mode": 1,
//       "param": "i",
//       "sliderMax": 100,
//       "sliderMin": 0,
//       "target": "Stage",
//       "visible": false,
//       "x": 5,
//       "y": 86
//     },
//     {
//       "cmd": "getVar:",
//       "color": 15629590,
//       "isDiscrete": true,
//       "label": "d1",
//       "mode": 1,
//       "param": "d1",
//       "sliderMax": 100,
//       "sliderMin": 0,
//       "target": "Stage",
//       "visible": false,
//       "x": 5,
//       "y": 113
//     }
//   ],
//   "costumes": [
//     {
//       "baseLayerID": -1,
//       "baseLayerMD5": "7623e679b2baa2e7d48808614820844f.svg",
//       "bitmapResolution": 1,
//       "costumeName": "blue sky2",
//       "rotationCenterX": 240,
//       "rotationCenterY": 180
//     },
//     {
//       "baseLayerID": -1,
//       "baseLayerMD5": "1e0f7a4c932423b13250b5cb44928109.svg",
//       "bitmapResolution": 1,
//       "costumeName": "woods",
//       "rotationCenterX": 240,
//       "rotationCenterY": 180
//     },
//     {
//       "baseLayerID": -1,
//       "baseLayerMD5": "108160d0e44d1c340182e31c9dc0758a.svg",
//       "bitmapResolution": 1,
//       "costumeName": "party",
//       "rotationCenterX": 251,
//       "rotationCenterY": 189
//     },
//     {
//       "baseLayerID": -1,
//       "baseLayerMD5": "de0e54cd11551566f044e7e6bc588b2c.png",
//       "bitmapResolution": 2,
//       "costumeName": "boardwalk",
//       "rotationCenterX": 480,
//       "rotationCenterY": 360
//     },
//     {
//       "baseLayerID": -1,
//       "baseLayerMD5": "2024d59c1980e667e8f656134796e2c1.svg",
//       "bitmapResolution": 1,
//       "costumeName": "blue sky3",
//       "rotationCenterX": 240,
//       "rotationCenterY": 179
//     },
//     {
//       "baseLayerID": -1,
//       "baseLayerMD5": "f339c6f31b11ea71d0fb8d607cec392e.png",
//       "bitmapResolution": 2,
//       "costumeName": "underwater1",
//       "rotationCenterX": 480,
//       "rotationCenterY": 360
//     },
//     {
//       "baseLayerID": -1,
//       "baseLayerMD5": "1517c21786d2d0edc2f3037408d850bd.png",
//       "bitmapResolution": 2,
//       "costumeName": "underwater2",
//       "rotationCenterX": 480,
//       "rotationCenterY": 360
//     },
//     {
//       "baseLayerID": -1,
//       "baseLayerMD5": "e87fed9c2a968dbeae8c94617e600e8c.png",
//       "bitmapResolution": 2,
//       "costumeName": "stars",
//       "rotationCenterX": 480,
//       "rotationCenterY": 360
//     },
//     {
//       "baseLayerID": -1,
//       "baseLayerMD5": "a7832479977c166ca0057f2a99a73305.png",
//       "bitmapResolution": 2,
//       "costumeName": "parking-ramp",
//       "rotationCenterX": 480,
//       "rotationCenterY": 360
//     },
//     {
//       "baseLayerID": 0,
//       "baseLayerMD5": "f67dc7de38bac6fbb0ab68e46352521d.png",
//       "bitmapResolution": 2,
//       "costumeName": "backdrop1",
//       "rotationCenterX": 480,
//       "rotationCenterY": 360
//     }
//   ],
//   "currentCostumeIndex": 9,
//   "info": {
//     "flashVersion": "MAC 23,0,0,207",
//     "hasCloudData": false,
//     "projectID": "119615668",
//     "scriptCount": 4,
//     "spriteCount": 1,
//     "swfVersion": "v452",
//     "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36",
//     "videoOn": false
//   },
//   "objName": "Stage",
//   "penLayerID": -1,
//   "penLayerMD5": "5c81a336fab8be57adc039a8a2b33ca9.png",
//   "scripts": [
//     [
//       92,
//       162,
//       [
//         [
//           "whenGreenFlag"
//         ],
//         [
//           "doForever",
//           [
//             [
//               "setGraphicEffect:to:",
//               "whirl",
//               [
//                 "*",
//                 10,
//                 [
//                   "readVariable",
//                   "x"
//                 ]
//               ]
//             ]
//           ]
//         ]
//       ]
//     ]
//   ],
//   "sounds": [
//     {
//       "format": "",
//       "md5": "83a9787d4cb6f3b7632b4ddfebf74367.wav",
//       "rate": 11025,
//       "sampleCount": 258,
//       "soundID": -1,
//       "soundName": "pop"
//     }
//   ],
//   "tempoBPM": 60,
//   "variables": [
//     {
//       "isPersistent": false,
//       "name": "vy",
//       "value": -3.5165000000000006
//     },
//     {
//       "isPersistent": false,
//       "name": "g",
//       "value": "-.5"
//     },
//     {
//       "isPersistent": false,
//       "name": "vx",
//       "value": 1.4580000000000002
//     },
//     {
//       "isPersistent": false,
//       "name": "i",
//       "value": 3
//     },
//     {
//       "isPersistent": false,
//       "name": "d1",
//       "value": 0
//     },
//     {
//       "isPersistent": false,
//       "name": "x",
//       "value": 237.52974211735977
//     }
//   ],
//   "videoAlpha": 0.5
// };
// var dataStr = JSON.stringify(data);

var loadProject = function () {
    // var id = location.hash.substring(1);
    // if (id.length < 1 || !isFinite(id)) {
    //     id = '119615668';
    // }
    // var url = 'https://projects.scratch.mit.edu/internalapi/project/' +
    //     id + '/get/';
    // var r = new XMLHttpRequest();
    // r.onreadystatechange = function() {
    //     if (this.readyState === 4) {
    //         if (r.status === 200) {
    //           window.vm.loadProject(this.responseText);
    //         }
    //     }
    // };
    // r.open('GET', url);
    // r.send();
    // window.vm.loadProject(dataStr);
};

window.onload = function() {
    // Lots of global variables to make debugging easier
    // Instantiate the VM.
    var vm = new window.VirtualMachine();
    window.vm = vm;

    // Loading projects from the server.
    document.getElementById('projectLoadButton').onclick = function () {
        document.location = '#' + document.getElementById('projectId').value;
        location.reload();
    };
    loadProject();

    // Instantiate the renderer and connect it to the VM.
    var canvas = document.getElementById('scratch-stage');
    var renderer = new window.RenderWebGL(canvas);
    window.renderer = renderer;
    vm.attachRenderer(renderer);

    // Instantiate scratch-blocks and attach it to the DOM.
    var workspace = window.Blockly.inject('blocks', {
        media: './media/',
        zoom: {
            controls: true,
            wheel: true,
            startScale: 0.75
        },
        colours: {
            workspace: '#334771',
            flyout: '#283856',
            scrollbar: '#24324D',
            scrollbarHover: '#0C111A',
            insertionMarker: '#FFFFFF',
            insertionMarkerOpacity: 0.3,
            fieldShadow: 'rgba(255, 255, 255, 0.3)',
            dragShadowOpacity: 0.6
        }
    });
    window.workspace = workspace;

    // Attach scratch-blocks events to VM.
    workspace.addChangeListener(vm.blockListener);
    var flyoutWorkspace = workspace.getFlyout().getWorkspace();
    flyoutWorkspace.addChangeListener(vm.flyoutBlockListener);

    // // Create FPS counter.
    var stats = new window.Stats();
    document.getElementById('tab-renderexplorer').appendChild(stats.dom);
    stats.dom.style.position = 'relative';
    stats.begin();

    // Playground data tabs.
    // Block representation tab.
    var blockexplorer = document.getElementById('blockexplorer');
    var updateBlockExplorer = function(blocks) {
        blockexplorer.innerHTML = JSON.stringify(blocks, null, 2);
        window.hljs.highlightBlock(blockexplorer);
    };

    // Thread representation tab.
    var threadexplorer = document.getElementById('threadexplorer');
    var cachedThreadJSON = '';
    var updateThreadExplorer = function (newJSON) {
        if (newJSON != cachedThreadJSON) {
            cachedThreadJSON = newJSON;
            threadexplorer.innerHTML = cachedThreadJSON;
            window.hljs.highlightBlock(threadexplorer);
        }
    };

    // Only request data from the VM thread if the appropriate tab is open.
    window.exploreTabOpen = false;
    var getPlaygroundData = function () {
        vm.getPlaygroundData();
        if (window.exploreTabOpen) {
            window.requestAnimationFrame(getPlaygroundData);
        }
    };

    // VM handlers.
    // Receipt of new playground data (thread, block representations).
    vm.on('playgroundData', function(data) {
        updateThreadExplorer(data.threads);
        updateBlockExplorer(data.blocks);
    });

    // Receipt of new block XML for the selected target.
    vm.on('workspaceUpdate', function (data) {
        workspace.clear();
        var dom = window.Blockly.Xml.textToDom(data.xml);
        window.Blockly.Xml.domToWorkspace(dom, workspace);
    });

    // Receipt of new list of targets, selected target update.
    var selectedTarget = document.getElementById('selectedTarget');
    vm.on('targetsUpdate', function (data) {
        // Clear select box.
        while (selectedTarget.firstChild) {
            selectedTarget.removeChild(selectedTarget.firstChild);
        }
        // Generate new select box.
        for (var i = 0; i < data.targetList.length; i++) {
            var targetOption = document.createElement('option');
            targetOption.setAttribute('value', data.targetList[i][0]);
            // If target id matches editingTarget id, select it.
            if (data.targetList[i][0] == data.editingTarget) {
                targetOption.setAttribute('selected', 'selected');
            }
            targetOption.appendChild(
                document.createTextNode(data.targetList[i][1])
            );
            selectedTarget.appendChild(targetOption);
        }
    });
    selectedTarget.onchange = function () {
        vm.setEditingTarget(this.value);
    };

    // Feedback for stacks and blocks running.
    vm.on('SCRIPT_GLOW_ON', function(data) {
        workspace.glowStack(data.id, true);
    });
    vm.on('SCRIPT_GLOW_OFF', function(data) {
        workspace.glowStack(data.id, false);
    });
    vm.on('BLOCK_GLOW_ON', function(data) {
        workspace.glowBlock(data.id, true);
    });
    vm.on('BLOCK_GLOW_OFF', function(data) {
        workspace.glowBlock(data.id, false);
    });
    vm.on('VISUAL_REPORT', function(data) {
        workspace.reportValue(data.id, data.value);
    });

    vm.on('SPRITE_INFO_REPORT', function(data) {
        document.getElementById('sinfo-x').value = data.x;
        document.getElementById('sinfo-y').value = data.y;
        document.getElementById('sinfo-direction').value = data.direction;
        document.getElementById('sinfo-rotationstyle').value = data.rotationStyle;
        document.getElementById('sinfo-visible').value = data.visible;
    });

    document.getElementById('sinfo-post').addEventListener('click', function () {
        var data = {};
        data.x = document.getElementById('sinfo-x').value;
        data.y = document.getElementById('sinfo-y').value;
        data.direction = document.getElementById('sinfo-direction').value;
        data.rotationStyle = document.getElementById('sinfo-rotationstyle').value;
        data.visible = document.getElementById('sinfo-visible').value === 'true';
        vm.postSpriteInfo(data);
    });

    // Feed mouse events as VM I/O events.
    document.addEventListener('mousemove', function (e) {
        var rect = canvas.getBoundingClientRect();
        var coordinates = {
            x: e.clientX - rect.left,
            y: e.clientY - rect.top,
            canvasWidth: rect.width,
            canvasHeight: rect.height
        };
        window.vm.postIOData('mouse', coordinates);
    });
    canvas.addEventListener('mousedown', function (e) {
        var rect = canvas.getBoundingClientRect();
        var data = {
            isDown: true,
            x: e.clientX - rect.left,
            y: e.clientY - rect.top,
            canvasWidth: rect.width,
            canvasHeight: rect.height
        };
        window.vm.postIOData('mouse', data);
        e.preventDefault();
    });
    canvas.addEventListener('mouseup', function (e) {
        var rect = canvas.getBoundingClientRect();
        var data = {
            isDown: false,
            x: e.clientX - rect.left,
            y: e.clientY - rect.top,
            canvasWidth: rect.width,
            canvasHeight: rect.height
        };
        window.vm.postIOData('mouse', data);
        e.preventDefault();
    });

    // Feed keyboard events as VM I/O events.
    document.addEventListener('keydown', function (e) {
        // Don't capture keys intended for Blockly inputs.
        if (e.target != document && e.target != document.body) {
            return;
        }
        window.vm.postIOData('keyboard', {
            keyCode: e.keyCode,
            isDown: true
        });
        e.preventDefault();
    });
    document.addEventListener('keyup', function(e) {
        // Always capture up events,
        // even those that have switched to other targets.
        window.vm.postIOData('keyboard', {
            keyCode: e.keyCode,
            isDown: false
        });
        // E.g., prevent scroll.
        if (e.target != document && e.target != document.body) {
            e.preventDefault();
        }
    });

    // Run threads
    vm.start();

    // Inform VM of animation frames.
    var animate = function() {
        stats.update();
        requestAnimationFrame(animate);
    };
    requestAnimationFrame(animate);

    // Handlers for green flag and stop all.
    document.getElementById('greenflag').addEventListener('click', function() {
        vm.greenFlag();
    });
    document.getElementById('stopall').addEventListener('click', function() {
        vm.stopAll();
    });

    // document.getElementById('turbomode').addEventListener('change', function() {
    //     var turboOn = document.getElementById('turbomode').checked;
    //     vm.setTurboMode(turboOn);
    // });
    // document.getElementById('compatmode').addEventListener('change',
    // function() {
    //     var compatibilityMode = document.getElementById('compatmode').checked;
    //     vm.setCompatibilityMode(compatibilityMode);
    // });
    var tabBlockExplorer = document.getElementById('tab-blockexplorer');
    var tabThreadExplorer = document.getElementById('tab-threadexplorer');
    var tabRenderExplorer = document.getElementById('tab-renderexplorer');
    var tabImportExport = document.getElementById('tab-importexport');

    // Handlers to show different explorers.
    document.getElementById('threadexplorer-link').addEventListener('click',
        function () {
            window.exploreTabOpen = true;
            getPlaygroundData();
            tabBlockExplorer.style.display = 'none';
            tabRenderExplorer.style.display = 'none';
            tabThreadExplorer.style.display = 'block';
            tabImportExport.style.display = 'none';
        });
    document.getElementById('blockexplorer-link').addEventListener('click',
        function () {
            window.exploreTabOpen = true;
            getPlaygroundData();
            tabBlockExplorer.style.display = 'block';
            tabRenderExplorer.style.display = 'none';
            tabThreadExplorer.style.display = 'none';
            tabImportExport.style.display = 'none';
        });
    document.getElementById('renderexplorer-link').addEventListener('click',
        function () {
            window.exploreTabOpen = false;
            tabBlockExplorer.style.display = 'none';
            tabRenderExplorer.style.display = 'block';
            tabThreadExplorer.style.display = 'none';
            tabImportExport.style.display = 'none';
        });
    document.getElementById('importexport-link').addEventListener('click',
        function () {
            window.exploreTabOpen = false;
            tabBlockExplorer.style.display = 'none';
            tabRenderExplorer.style.display = 'none';
            tabThreadExplorer.style.display = 'none';
            tabImportExport.style.display = 'block';
        });

    // customized
    var show = true;
    $('#toggleFlyout').on('click', function() {
      if(show) {
        show = false;
        $('.blocklyToolboxDiv').hide();
        $('.blocklyFlyout').hide();
        $('.blocklyFlyout').css('transform', 'translateX(-300px)');
        $('.blocklyToolboxDiv').css('transform', 'translateX(-300px)');
      } else {
        show = true;
        $('.blocklyToolboxDiv').show();
        $('.blocklyFlyout').show();
        $('.blocklyToolboxDiv').css('transform', 'translateX(0)');
        $('.blocklyFlyout').css({
          'transform':'translate(0, 170px)'
        });
      }
    });

    // 加载xml到项目中
    var xml = fetchFile('./xml/linefollow.xml');
    var output = document.getElementById('importExport');
    output.value = xml
    setTimeout(function() {
      fromXml();
    }, 500)


    vm.createEmptyProject();
    console.log(2);
    vm.setEditingTarget(vm.runtime.targets[1].id);
    vm.runtime.targets[1].setCostume(1);
    vm.runtime.targets[1].setSize("50");
    vm.runtime.targets[0].setCostume(1);

    // 开启sensor
    MBlockly.Action.openSensorDetecting();

    var openSensor = true;
    $('#sensorBtn').on('click', function() {
      if(!openSensor) {
        openSensor = true;
        if($('#sensorDelay')) {
          var delay = $('#sensorDelay').val();
          MBlockly.Action.openSensorDetecting(delay);
        }
      } else {
        openSensor = false;
        console.log('stop sensors');
        MBlockly.Action.stopSensorDetecting();
      }
    });
};
