/**
 * @description action.js - define actions and dispose methods in control.js
 * @author Hujinhong
 * @copyright Makeblock.cc
 */

MBlockly.Action = {
  ultrasonicTimer : null,
  lightTimer : null,
  lineFollowerTimer : null,
  delay: 20,
  runSpeed : function(speed, dir) {
    // 左右电机相反
    var spd1 = -dir * speed;
    var spd2 = dir * speed;
    MBlockly.Control.setSpeed(spd1, spd2);
  },

  turnSpeed : function(speed, dir) {
    var spd1, spd2;
    if(dir == 1) {
        spd1 = speed;
        spd2 = speed;
    } else {
        spd1 = -1 * speed;
        spd2 = -1 * speed;
    }
    MBlockly.Control.setSpeed(spd1, spd2);
  },

  openSensorDetecting: function(delay) {
    delay = delay || this.delay;
    console.log(delay);
    // this.ultrasonicTimer = setInterval(function() {
    //   MBlockly.Control.getSensorValue('ULTRASONIC', 3, function(result){
    //     MBlockly.Cache.ultrasonic = result;
    //     $('.ultrasonic').text(result);
    //   })
    // }, this.delay);

    // this.lightTimer = setInterval(function() {
    //   MBlockly.Control.getSensorValue('LIGHTSENSOR', 6, function(result){
    //     MBlockly.Cache.lightness = result;
    //     $('.lightsensor').text(result);
    //   })
    // }, this.delay + 20);

    this.lineFollowerTimer = setInterval(function() {
      MBlockly.Control.getSensorValue('LINEFOLLOW', 2, function(result){
        MBlockly.Cache.linefollower = result;
        $('.linefollower').text(result);
      })
    }, this.delay);
  },

  stopSensorDetecting: function() {
    clearInterval(this.ultrasonicTimer);
    clearInterval(this.lightTimer);
    clearInterval(this.lineFollowerTimer);
  }
};