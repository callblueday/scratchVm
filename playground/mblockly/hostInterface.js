var MBlockly = {};

function arrayBufferToArray(buffer){
    var dataView = new Uint8Array(buffer);
    var result = [];
    for(var i=0;i<dataView.length;i++){
        result.push(dataView[i]);
    }
    return result;
}

MBlockly.HostInterface = {
  send: function(a) {
    // send
    // console.log(a);
    if(socket) {
      socket.emit('send', a);
    }
  },

  sendBluetoothRequestUnrelibly: function(a) {
    this.send(a);
  },

  sendBluetoothRequest: function(a) {
    this.send(a);
  },

  receiveData: function(data) {
    var result = arrayBufferToArray(data);
    if(typeof result != 'undefined' && result.length > 4) {
      // receive
      MBlockly.Control.decodeData(result);
    }
  }
};