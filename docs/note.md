1.定义mbot动作
  - 运动块
  - 超声波块
  - 颜色块
2. scratch-vm中增加新的block块
3. 运行新的block块
  - 运行block块
4. 将block块烧录进主板。通过ipad上传
5. 主板解析block块
6. 主板发控制mbot协议指令
7. 小车运动


## scratch3.0
- blocks/ 定义基本块的动作


获取工作区的block块

var xml = Blockly.Xml.workspaceToDom(workspace);


## 示例目标
wifi连接
scratch-vm在跑的同时，小车也在下面跑，断开连接，小车自主在跑。

比如巡线：
小车在巡线的时候，不断上报数据，上位机获取当前状态，实时显示，“向左转”，“向右转”。


### 步骤
1. [x]实现mbot+edison的串口连接
2. [x]调试edison与mbot的串口通信
3. [x]在edison上跑一个server，跑一个socketio，暴露写串口和读串口数据的接口
4. 将scratch-vm运行在edison上
5. [x]给block块定义控制指令
6. 定义读值指令
6. 读值显示在舞台上。

### 添加单张sprite图
改变默认舞台的图，添加mbot的图以及实例代码。
VirtualMachine.prototype.addSprite2  (/src/index.js)

加载一个项目
vm.loadProject(jsonStr)

加一个sprite图
vm.addSprite2(jsonStr)

切换target：
vm.setEditingTarget(targetId);

vm的runtime是在管理target、scripts、sequencer

如何保存一个项目的原始数据

每一个角色都是一个 sprite 对象，必备属性：

name: 'projectName',
costumes : [
    {
        skin: './assets/mbot.png',
        name: 'backdrop1',
        bitmapResolution: 2,
        rotationCenterX: 480,
        rotationCenterY: 360
    }
]

如果skin路径找不到，会自动去下载一个默认的？
该设置项在： `/src/import/sb2import.js`

https://cdn.assets.scratch.mit.edu/internalapi/asset/7e24c99c1b853e52f8e7f9004416fa34.png/get/

通过以下方法
vm.runtime.targets[1].setCostume(1);
来处理默认图片的替换。


x的范围： -200,230
y的范围： 160,-100